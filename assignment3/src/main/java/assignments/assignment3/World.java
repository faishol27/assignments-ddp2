package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        // Constructor untuk class World
        this.listCarrier = new ArrayList<Carrier>();
    }

    public Carrier createObject(String tipe, String nama){
        switch (tipe) {
            case "OJOL":
                listCarrier.add(new Ojol(nama));
                break;
            case "PEKERJA_JASA":
                listCarrier.add(new PekerjaJasa(nama));
                break;
            case "JURNALIS":
                listCarrier.add(new Jurnalis(nama));
                break;
            case "PETUGAS_MEDIS":
                listCarrier.add(new PetugasMedis(nama));
                break;
            case "CLEANING_SERVICE":
                listCarrier.add(new CleaningService(nama));
                break;
            case "PEGANGAN_TANGGA":
                listCarrier.add(new PeganganTangga(nama));
                break;
            case "ANGKUTAN_UMUM":
                listCarrier.add(new AngkutanUmum(nama));
                break;
            case "TOMBOL_LIFT":
                listCarrier.add(new TombolLift(nama));
                break;
            case "PINTU":
                listCarrier.add(new Pintu(nama));
                break;
        }
        return null;
    }

    public Carrier getCarrier(String nama){
        // Mengambil object carrier dengan nama sesuai dengan parameter
        for (Carrier elm:listCarrier){
            if (elm.getNama().equalsIgnoreCase(nama)) return elm;
        }
        return null;
    }
}
