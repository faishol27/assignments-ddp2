package assignments.assignment3;

public class Positif implements Status{
  
    public String getStatus(){
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular){
        // Mengimplementasikan apabila object Penular melakukan interaksi dengan object tertular
        String tipePenular = penular.getTipe();
        String tipeTertular = tertular.getTipe();
        String statusTertular = tertular.getStatusCovid();
        
        if (statusTertular.equals("Positif")) return;
        
        if (tipeTertular.equals("MANUSIA")) {
            // Interaksi Manusia - Manusia dan Benda - Manusia
            tertular.ubahStatus("Positif");
            tertular.buildRantaiPenular(penular);
        } else {
            if (tipePenular.equals("MANUSIA")) {
                // Interaksi Manusia - Benda
                
                ((Benda) tertular).tambahPersentase();
                if (tertular.getStatusCovid().equals("Positif")) {
                    tertular.buildRantaiPenular(penular);
                }
            }
        }
    }
}