package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama,String tipe){
        // Constructor untuk Carrier.
        this.nama = nama;
        this.tipe = tipe;
        statusCovid = new Negatif();
        aktifKasusDisebabkan = 0;
        totalKasusDisebabkan = 0;
        rantaiPenular = new ArrayList<Carrier>();
    }

    public String getNama(){
        // Mengembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe(){
        // Mengembalikan nilai dari atribut tipe
        return this.tipe;
    }

    public String getStatusCovid(){
        // Mengembalikan nilai dari atribut statusCovid
        return this.statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        // Mengembalikan nilai dari atribut aktifKasusDisebabkan
        return this.aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan(){
        // Mengembalikan nilai dari atribut totalKasusDisebabkan
        return this.totalKasusDisebabkan;
    }

    public List<Carrier> getRantaiPenular(){
        // Mengembalikan nilai dari atribut rantaiPenular
        return this.rantaiPenular;
    }
    
    public void ubahStatus(String status){
        // Mengimplementasikan fungsi ini untuk mengubah atribut dari statusCovid
        if (status.equals("Positif") == true) {
            this.statusCovid = new Positif();
        } else {
            if (this.statusCovid.getStatus().equals("Positif")) {
                // Mengurangi aktifKasusDisebabkan untuk setiap penular dari object bersangkutan
                for (Carrier penular:rantaiPenular){
                    penular.setAktifKasusDisebabkan(penular.getAktifKasusDisebabkan() - 1);
                }
                rantaiPenular.clear();
            }
            this.statusCovid = new Negatif();
        }
    }

    public void interaksi(Carrier lain){
        // Objek ini berinteraksi dengan objek lain
        this.statusCovid.tularkan(this, lain);
    }

    public void setAktifKasusDisebabkan(int val){
        // setter nilai dari atribut aktifKasusDisebabkan
        this.aktifKasusDisebabkan = val;
    }

    public void addKasusDisebabkan(){
        // Menambah nilai dari atribut totalKasusDisebabkan dan aktifKasusDisebabkan
        this.totalKasusDisebabkan++;
        this.aktifKasusDisebabkan++;
    }

    public void addRantaiPenular(Carrier penular){
        // Menambah elemen ke atribut rantaiPenular
        this.rantaiPenular.add(penular);
    }

    public void buildRantaiPenular(Carrier penular){
        // Mengisi atribut rantaiPenular
        Set<Carrier> bef = new HashSet<Carrier>();

        for (Carrier rantai:penular.getRantaiPenular()) {
            this.addRantaiPenular(rantai);
            if (bef.contains(rantai) == false) {
                rantai.addKasusDisebabkan();
                bef.add(rantai);
            }
        }

        this.addRantaiPenular(penular);
        if (bef.contains(penular) == false) {
            penular.addKasusDisebabkan();
        }
    }

    public abstract String toString();
    
}
