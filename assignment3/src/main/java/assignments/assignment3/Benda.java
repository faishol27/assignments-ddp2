package assignments.assignment3;


public abstract class Benda extends Carrier{
  
    protected int persentaseMenular;

    public Benda(String nama){
        // Constructor untuk Benda.
        super(nama, "BENDA");
        persentaseMenular = 0;
    }

    public abstract void tambahPersentase();

    public int getPersentaseMenular(){
        // Mengembalikan nilai dari atribut persentaseMenular
        return persentaseMenular;
    }
    
    public void setPersentaseMenular(int persentase) {
        // Setter untuk atribut persentase menular
        this.persentaseMenular = persentase;
        if (this.persentaseMenular == 0) {
            this.ubahStatus("Negatif");
        }else if(this.persentaseMenular >= 100) {
            this.ubahStatus("Positif");
        }
    }
}