package assignments.assignment3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputType;
    private String outputType;
    private String inputFile;
    private String outputFile; 
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile){
        // Constructor untuk InputOutput.
        this.inputType = inputType;
        this.outputType = outputType;
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }

    public void setBufferedReader(String inputType) throws IOException{
        // Membuat BufferedReader bergantung inputType (I/O text atau input terminal) 
        if (inputType.equalsIgnoreCase("terminal")) {
            br = new BufferedReader(new InputStreamReader(System.in));
        } else {
            br = new BufferedReader(new FileReader(this.inputFile));
        }
    }
    
    public void setPrintWriter(String outputType) throws IOException{
        // Membuat PrintWriter bergantung inputType (I/O text atau output terminal)
        if (outputType.equalsIgnoreCase("terminal")) {
            pw = new PrintWriter(System.out);
        } else {
            pw = new PrintWriter(this.outputFile);
        }
    }
    
    public void initiateWorld(){
        world = new World();
    }

    public void run() throws IOException{
        // Program utama untuk InputOutput, jangan lupa handle untuk IOException
        setBufferedReader(inputType);
        setPrintWriter(outputType);

        initiateWorld();
        while(true){
            String inp = br.readLine();
            String[] query = inp.split(" ");
            
            if (query[0].equalsIgnoreCase("EXIT")) {
                break;
            } else {
                String ret = parseQuery(query);
                if (ret != "") {
                    pw.println(ret);
                    pw.flush();
                }
            }
        }
    }
    
    public String parseQuery(String[] query){
        // Melakukan parsing pada query
        String res = "";

        if (query[0].equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")) {
            res = String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus",
                    Manusia.getJumlahSembuh());
        } else if (query[0].equalsIgnoreCase("ADD")) {
            world.createObject(query[1], query[2]);
        } else if (query.length == 2) {
            Carrier carrier1 = world.getCarrier(query[1]);
            
            if (query[0].equalsIgnoreCase("RANTAI")) {
                res = getPrintRantai(carrier1);
            } else if (query[0].equalsIgnoreCase("POSITIFKAN")) {
                carrier1.ubahStatus("Positif");
            } else if (query[0].equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) {
                res = String.format("%s telah menyebarkan virus COVID ke %d objek",
                        carrier1, carrier1.getTotalKasusDisebabkan());
            } else if (query[0].equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) {
                res = String.format("%s telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak %d objek",
                        carrier1, carrier1.getAktifKasusDisebabkan());
            } else if (query[0].equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                res = String.format("%s menyembuhkan %d manusia", carrier1,
                        ((PetugasMedis) carrier1).getJumlahDisembuhkan());
            } else if (query[0].equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) {
                res = String.format("%s membersihkan %d benda", carrier1,
                        ((CleaningService) carrier1).getJumlahDibersihkan());
            }
        } else if (query.length == 3) {
            Carrier carrier1 = world.getCarrier(query[1]);
            Carrier carrier2 = world.getCarrier(query[2]);
            
            if (query[0].equalsIgnoreCase("INTERAKSI")) {
                carrier1.interaksi(carrier2);
                carrier2.interaksi(carrier1);
            }  else if (query[0].equalsIgnoreCase("SEMBUHKAN")) {
                carrier1 = world.getCarrier(query[1]);
                carrier2 = world.getCarrier(query[2]);
                ((PetugasMedis) carrier1).obati((Manusia) world.getCarrier(query[2]));
            } else if (query[0].equalsIgnoreCase("BERSIHKAN")) {
                ((CleaningService) carrier1).bersihkan((Benda) world.getCarrier(query[2]));
            } 
        }

        return res;
    }

    public String getPrintRantai(Carrier carrier){
        // Mencetak rantai penularan sesuai format
        String res = String.format("Rantai penyebaran %s: ", carrier);
        try{
            if (carrier.getStatusCovid().equals("Negatif")) {
                String msg = String.format("%s berstatus negatif", carrier);
                throw new BelumTertularException(msg);
            }

            List <Carrier> rantaiPenular = carrier.getRantaiPenular();
            for (Carrier penular:rantaiPenular) {
                res += String.format("%s -> ", penular);
            }
            res += carrier;
        }catch(BelumTertularException e){
            res = e.toString();
        }
        return res;
    }
    
}