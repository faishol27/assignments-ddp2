package assignments.assignment3;

public class AngkutanUmum extends Benda{
      
    public AngkutanUmum(String nama){
        // Constructor untuk AngkutanUmum.
        super(nama);
    }

    public void tambahPersentase(){
        // Menambah persentaseMenular sesuai spesifikasi soal
        this.setPersentaseMenular(this.getPersentaseMenular() + 35);
    }
    
    public String toString(){
        return String.format("ANGKUTAN UMUM %s", this.getNama());
    }
}