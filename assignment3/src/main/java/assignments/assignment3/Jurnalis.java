package assignments.assignment3;

public class Jurnalis extends Manusia{
  	
    public Jurnalis(String nama){
        // Constructor untuk Jurnalis.
        super(nama);     
    }

    @Override
    public String toString(){
        return String.format("JURNALIS %s", this.getNama());
    }
}