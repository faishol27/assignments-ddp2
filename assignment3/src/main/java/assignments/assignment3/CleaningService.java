package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama){
        // Constructor untuk CleaningService.
        super(nama);
        jumlahDibersihkan = 0;
    }

    public void bersihkan(Benda benda){
        // Mengimplementasikan apabila objek CleaningService ini membersihkan benda
        benda.setPersentaseMenular(0);
        this.jumlahDibersihkan++;
    }

    public int getJumlahDibersihkan(){
        // Mengembalikan nilai dari atribut jumlahDibersihkan
        return this.jumlahDibersihkan;
    }

    @Override
    public String toString(){
        return String.format("CLEANING SERVICE %s", this.getNama());
    }

}