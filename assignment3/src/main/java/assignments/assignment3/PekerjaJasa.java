package assignments.assignment3;

public class PekerjaJasa extends Manusia{
  	
    public PekerjaJasa(String nama){
    	// Constructor untuk Jurnalis.
        super(nama);
    }
    
    @Override
    public String toString(){
        return String.format("PEKERJA JASA %s", this.getNama());
    }
}