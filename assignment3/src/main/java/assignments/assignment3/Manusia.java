package assignments.assignment3;

public abstract class Manusia extends Carrier{
    private static int jumlahSembuh = 0;
    
    public Manusia(String nama){
        // Constructor untuk Manusia.
        super(nama, "MANUSIA");
    }
    
    public void tambahSembuh(){
        // Menambahkan nilai pada atribut jumlahSembuh.
        jumlahSembuh++;
    }

    public static int getJumlahSembuh() {
        // Mengembalikan nilai pada atribut jumlahSembuh.
        return jumlahSembuh;
    }
    
}