package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String nama){
        // Constructor untuk Jurnalis.
        super(nama);
        jumlahDisembuhkan = 0;
    }

    public void obati(Manusia manusia) {
        // Mengimplementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        manusia.ubahStatus("Negatif");
        this.jumlahDisembuhkan++;
        this.tambahSembuh();
    }

    public int getJumlahDisembuhkan(){
        // Mengembalikan nilai dari atribut jumlahDisembuhkan
        return this.jumlahDisembuhkan;
    }

    @Override
    public String toString(){
        return String.format("PETUGAS MEDIS %s", this.getNama());
    }
}