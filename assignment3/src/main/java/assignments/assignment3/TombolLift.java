package assignments.assignment3;

public class TombolLift extends Benda{
       
    public TombolLift(String nama){
        // Constructor untuk class TombolLift
        super(nama);
    }

    public void tambahPersentase(){
        // Menambah persentaseMenular sesuai spesifikasi soal
        this.setPersentaseMenular(this.getPersentaseMenular() + 20);
    }
    
    public String toString(){
        return String.format("TOMBOL LIFT %s", this.getNama());
    }
}