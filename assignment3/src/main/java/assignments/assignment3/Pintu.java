package assignments.assignment3;

public class Pintu extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda  
    
    public Pintu(String nama){
        // Constructor class Pintu
        super(nama);
    }

    public void tambahPersentase(){
        // Menambah persentaseMenular sesuai spesifikasi soal
        this.setPersentaseMenular(this.getPersentaseMenular() + 30);
    }
    
    public String toString(){
        return String.format("PINTU %s", this.getNama());
    }
}