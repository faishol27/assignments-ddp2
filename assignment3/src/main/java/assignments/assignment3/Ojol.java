package assignments.assignment3;

public class Ojol extends Manusia{
  	
    public Ojol(String nama){
		// Constructor untuk Jurnalis.
        super(nama);
    }
      
    @Override
    public String toString(){
        return String.format("OJOL %s", this.getNama());
    }
}