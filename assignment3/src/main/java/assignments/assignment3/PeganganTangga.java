package assignments.assignment3;

public class PeganganTangga extends Benda{
    
    public PeganganTangga(String nama){
        // Constructor untuk Jurnalis.
        super(nama);
    }


    public void tambahPersentase(){
        // Menambah persentaseMenular sesuai spesifikasi soal
        this.setPersentaseMenular(this.getPersentaseMenular() + 20);
    }
    
    public String toString(){
        return String.format("PEGANGAN TANGGA %s", this.getNama());
    }
    
}