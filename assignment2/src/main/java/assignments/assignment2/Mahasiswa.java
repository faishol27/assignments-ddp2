package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for (KomponenPenilaian elm:komponenPenilaian) {
            if (elm == null) continue;
            if (elm.getNama().equalsIgnoreCase(namaKomponen) == true) {
                return elm;
            }
        }
        return null;
    }

    public String getNpm() {
        return this.npm;
    }
    
    public double getNilaiAkhir() {
        double res = 0.0;
        for (KomponenPenilaian elm:komponenPenilaian) {
            if (elm == null) continue;
            res += elm.getNilai();
        }
        return res;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }
    
    private String rekapFooter() {
        String res = "";
        double nilaiAkhir = getNilaiAkhir();
        res += String.format("Nilai akhir: %.2f\n", nilaiAkhir);
        res += String.format("Huruf: %s\n", getHuruf(nilaiAkhir));
        res += getKelulusan(nilaiAkhir);
        return res;
    }

    public String rekap() {
        int sz = komponenPenilaian.length;
        String res = "";
        for (int i = 0; i < sz; i++) {
            KomponenPenilaian elm = komponenPenilaian[i];
            if (elm == null) continue;
            res += elm.toString() + "\n";
        }
        res += rekapFooter();
        return res;
    }

    public String toString() {
        return String.format("%s - %s", this.npm, this.nama);
    }

    public String getDetail() {
        String res = "";
        for (KomponenPenilaian elm:komponenPenilaian) {
            if (elm == null) continue;
            res += elm.getDetail() + "\n\n";
        }
        res += rekapFooter();
        return res;
    }

    @Override
    public int compareTo(Mahasiswa other) {
        return this.npm.compareTo(other.getNpm());
    }
}
