package assignments.assignment2;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    public void deleteMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.remove(mahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        for (Mahasiswa elm:mahasiswa) {
            if (elm.getNpm().equalsIgnoreCase(npm) == true) {
                return elm;
            }
        }
        return null;
    }

    public String rekap() {
        int sz = mahasiswa.size();
        String res = "";
        for (int i = 0; i < sz; i++) {
            Mahasiswa elm = mahasiswa.get(i);
            res += elm.toString() + "\n";
            res += elm.rekap();
            if (i < sz - 1) {
                res += "\n\n";
            }
        }
        return res;
    }

    public String toString() {
        return String.format("%s - %s", this.kode, this.nama);
    }
}
