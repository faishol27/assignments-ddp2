package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama = nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot = bobot;
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        this.butirPenilaian[idx] = butir;
    }

    public String getNama() {
        return this.nama;
    }

    public double getRerata() {
        double sumNilai = 0;
        int banyakNonEmpty = 0;
        
        for (ButirPenilaian elm:butirPenilaian) {
            if (elm != null) {
                sumNilai += elm.getNilai();
                banyakNonEmpty++;
            }
        }
        if (banyakNonEmpty != 0) {
            return sumNilai / (double) banyakNonEmpty;
        } 
        return 0.0;
    }

    public double getNilai() {
        return getRerata() * (double) bobot / 100.0;
    }

    public String getDetail() {
        String res = "";
        int banyakButirPenilaian = butirPenilaian.length;
        res += String.format("~~~ %s (%d%%) ~~~\n", this.nama, this.bobot);
        
        if (banyakButirPenilaian == 1) {
            String score = "0.00";
            if (butirPenilaian[0] != null) {
                score = butirPenilaian[0].toString();
            }
            res += String.format("%s: %s\n", this.nama, score);
        } else {
            for (int i = 0; i < banyakButirPenilaian; i++) {
                if (butirPenilaian[i] != null) {
                    res += String.format("%s %d: %s\n", this.nama, i + 1, butirPenilaian[i].toString());
                }
            }
            res += String.format("Rerata: %.2f\n", getRerata());
        }
        
        res += String.format("Kontribusi nilai akhir: %.2f", getNilai());
        return res;
    }

    @Override
    public String toString() {
        return String.format("Rerata %s: %.2f", this.nama, getRerata());
    }

}
