package assignments.assignment2;

import java.lang.Math;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        this.nilai = Math.max(nilai, 0.0);
        this.terlambat = terlambat;
    }

    public double getNilai() { 
        if (this.terlambat == true) {
            return (100.0 - PENALTI_KETERLAMBATAN) / 100.0 * nilai;
        }
        return nilai;
    }

    @Override
    public String toString() {
        String res = String.format("%.2f", getNilai());
        if (this.terlambat == true) {
            res = res + " (T)";
        }
        return res;
    }
}
