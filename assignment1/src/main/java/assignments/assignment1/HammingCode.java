package assignments.assignment1;

import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;
    
    /**
     * Function to get number of redundant that must be added to data.
     * @param len (int) Length of the data
     */
    public static int getNumRedundan(int len) {
        for (int r = 0; r < 64; r++) {
            int powerTwo = (1 << r);
            if (powerTwo >= len + r + 1) {
                return r;
            }
        }
        return 64;
    }

    /**
     * Function to encode data to Hamming Code.
     * @param data (String) Data that will be encoded
     */
    public static String encode(String data) {
        int idxData = 0;
        int numRedundant = getNumRedundan(data.length());
        int dataBitLen = numRedundant + data.length() + 1;
        String res = "";
        char[] dataBit = new char[dataBitLen + 5];
        // Initiate dataBit with data and all parity bit set to 0
        for (int i = 1; idxData < data.length(); i++) {
            if ((i & (i - 1)) != 0) {
                dataBit[i] = data.charAt(idxData++);
            } else {
                dataBit[i] = '0';
            }
        }
        // Looping for every parity bits in position 1, 2, 4, ..., 2 ^ (numRedundant - 1)
        for (int i = 0; i < numRedundant; i++) {
            int powerTwo = (1 << i);
            int cntBitOne = 0;
            // Counting number of bit one
            for (int j = powerTwo; j < dataBitLen; j += (powerTwo << 1)) {
                for (int k = 0; k < powerTwo && j + k < dataBitLen; k++) {
                    if (dataBit[j + k] == '1') {
                        cntBitOne++;
                    }
                }
            }
            // Toggling parity bits
            if (cntBitOne % 2 == 1) {
                dataBit[powerTwo] = '1'; 
            }
        }
        // Append encoded data to accumulator
        for (int i = 1; i < dataBitLen; i++) {
            res += dataBit[i];
        }
        return res;
    }

    /**
     * Function to decode and repair Hamming Code.
     * @param code (String) code that will be decoded
     */
    public static String decode(String code) {
        String res = "";
        int brokenIdx = -1;
        int codeLen = code.length();
        // Validating the parity bits
        for (int i = 1; i <= codeLen; i++) {
            // Only check bit in position power of two
            if ((i & (i - 1)) == 0) {
                int idx = i - 1;
                int cntBitOne = 0;
                // Counting number of bit one, including parity bits
                for (int j = idx; j < codeLen; j += 2 * i) {
                    for (int k = 0; k < i && j + k < codeLen; k++) {
                        if (code.charAt(j + k) == '1') {
                            cntBitOne++;
                        }
                    }
                }
                if (cntBitOne % 2 != 0) {
                    if (brokenIdx == -1) {
                        brokenIdx = i;
                    } else {
                        brokenIdx += i;
                    }
                }
            }
        }
        // Get the real data
        for (int i = 1; i <= codeLen; i++) {
            // Ignoring the parity bits
            if ((i & (i - 1)) == 0) {
                continue;
            }
            if (brokenIdx == i) {
                // Repair the broken bits and append to accumulator
                if (code.charAt(brokenIdx - 1) == '1') {
                    res += '0';
                } else {
                    res += '1';
                }
            } else {
                // Append correct bit to accumulator
                res += code.charAt(i - 1);
            }
        }
        return res;
    }

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
